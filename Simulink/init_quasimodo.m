clear
close all
clc

% Expand path
addpath('src');
addpath('wr-actuator-library');
addpath('wr-actuator-library/ParameterFiles');
% `WE2_library_init` will add the remaining paths

% Call init script of library
WE2_library_init;

% Clear some things we don't need, to limit cluttering
clearvars WE_Backpack_params WE_BackpackBus BackpackBus JointParams ...
    WE_ForcePlateBus WE_IMUBus forceplate SilverTestActuator

% Load our own parameter file
load_quasimodo_params;


%% High-level Controller Parameters

ControlParams = struct;

% Variable cannot be used in model, since that will replace the
% parameters with a bus
ControlParams.Mode = uint8(0);
ControlParams.MaxTorque = double(zeros(1, 4));
ControlParams.ROM_Min = double(zeros(1, 4));
ControlParams.ROM_Max = double(zeros(1, 4));
ControlParams.MaxRefVel = 0.0;
ControlParams.Kp = 0.0;
ControlParams.Kd = 0.0;

ControlParamsBus = struct2bus(ControlParams, 'ControlParamsBus');
